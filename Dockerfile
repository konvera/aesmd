FROM ubuntu:20.04

ENV AESM_PATH=/opt/intel/sgx-aesm-service/aesm
ENV LD_LIBRARY_PATH=$AESM_PATH

RUN apt update && \
    apt install -y libssl-dev gnupg software-properties-common

RUN apt-key adv --fetch-keys https://packages.microsoft.com/keys/microsoft.asc && \
    apt-add-repository 'https://packages.microsoft.com/ubuntu/20.04/prod main' && \
    apt-key adv --fetch-keys https://download.01.org/intel-sgx/sgx_repo/ubuntu/intel-sgx-deb.key && \
    add-apt-repository 'https://download.01.org/intel-sgx/sgx_repo/ubuntu main'

RUN apt-get update && apt-get install -y az-dcap-client sgx-aesm-service && \
    apt-get clean -y && apt-get autoclean -y && apt-get autoremove -y && \
    /bin/mkdir -p /var/run/aesmd/ && \
    /bin/chown -R aesmd:aesmd /var/run/aesmd/ && \
    /bin/chmod 0755 /var/run/aesmd/ && \
    /bin/chown -R aesmd:aesmd /var/opt/aesmd/ && \
    /bin/chmod 0750 /var/opt/aesmd/


WORKDIR $AESM_PATH

CMD ["/bin/bash", "-c", "$AESM_PATH/aesm_service --no-daemon"]
